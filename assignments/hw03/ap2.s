    .set noreorder
    .data
	A: .word 1, 25, 7, 9, -1
    .text
    .globl main
    .ent main
main:
    lui $t0, %hi(A)
    ori $t0, $t0, %lo(A)

    addi $s0, $0, 0	#$s0=i=0
    add $t1, $t0, $s0	
    lw $s1, 0($t1)	#load $s1 into memory $t1+0; $s1=current
    addi $s2, $0, 0	#$s2=max, default it to 0

loop:
    bltz $s1, exit	#branch if $s1 less than 0
    slt $t2, $s1, $s2	#if current is less than max, $t2=1
    beq $t2, $0, L1	#if $t2=0, go to L1
    nop

loop_cont:
    addi $s0, $s0, 4	#$s0=i, increment i
    add $t3, $t0, $s0	#add increment to $t0
    lw $s1, 0($t3)	#get new current value
    j loop		#return to loop
    nop

L1:
    add $s2, $0, $s1	#set current to new max
    j loop_cont		#continue rest of loop

exit:
    add $a0, $0, $s2
    ori $v0, $0, 20	#print hex/dec
    syscall

    ori $v0, $0, 10	#exit
    syscall

    .end main

