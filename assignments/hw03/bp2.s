    .set noreorder
    .data
    .text
    .globl print
    .ent print

print:
    ori $v0, $0, 20	#print hex/dec
    syscall

    jr $ra		#jump to register $ra
    nop

    .end print

    .globl sum3
    .ent sum3
sum3:
    add $t7, $a0, $a1	#add $a0 and $a0 
    add $t7, $t7, $a2	#add $a2 to above

    add $a0, $0, $t7	#$a0=$t7

    ori $v0, $0, 20	#syscall for print hex/dec
    jr $ra		#jump to register $ra
    nop

    .end sum3

    .globl polynomial
    .ent polynomial
polynomial:
    sllv $t0, $a0, $a1	#$t0=x -> x=a<<b
    sllv $t1, $a2, $a3	#$t1=y -> y=c<<d
    addi $a0, $t0, 0	#set values of $a0, $a1, $a2 to x, y, e 
    addi $a1, $t1, 0
    addi $a2, $t4, 0
    jal sum3		#get sum of $a0, $a1, $a2 (x, y ,e)
    nop

    addi $t2, $a0, 0
    addi $a0, $t0, 0
    jal print 		#print x
    nop

    addi $a0, $t1, 0
    jal print		#print y
    nop

    addi $a0, $t2, 0
    jal print		#print z
    nop

    addi $a0, $t2, 0
    jr $t9		#return z
    nop

    .end polynomial

    .globl main
    .ent main
main:
    addi $s0, $0, 2	#$s0=a=2
    add $a0, $0, $s0	#$a0=$s0
    addi $a1, $0, 3	#set a1, a2, a3, t4 to 3, 4, 5, 6
    addi $a2, $0, 4
    addi $a3, $0, 5
    addi $t4, $0, 6
    jal polynomial	#calculate polynomial of a, 3, 4, 5, 6

    addi $t9, $ra, 0
    addi $t5, $a0, 0
    addi $a0, $s0, 0
    jal print		#print a
    nop

    addi $a0, $t5, 0
    jal print		#print f
    nop

    ori $v0, $0, 10	#exit
    syscall

    .end main



