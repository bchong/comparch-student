    .set noreorder
    .data
    A: .word 0:8
    .text
    .globl main
    .ent main

main:
    lui $t0, %hi(A)
    ori $t0, $t0, %lo(A)

    addi $t1, $0, 1
    sw $0, 0($t0)	#A[0]=0
    sw $t1, 4($t0)	#A[1]=1
    addi $s0, $0, 0	#$s0=i=0

loop:
    slti $t2, $s0, 6 	#if $s0=i less than 6, $t2=1
    beq $t2, $0, end	#if $t2 is equal to 0, go to end
    nop
    sll $t3, $s0, 2	#shift $s0 left by 2 and save into $t3
    add $t4, $t0, $t3	#add shifted $t3 to #t0
    lw $t5, 0($t4) 	#$t5=A[i-1]
    lw $t6, 4($t4)	#$t6=A[i-2]
    add $t7, $t6, $t5	#$t7=A[i]
    sw $t7, 8($t4)	#store A[i] in correct position in array
    add $a0, $0, $t7
    ori $v0, $0, 20	#print hex/dec
    syscall
    addi $s0, $s0, 1	#increment i    		
    j loop		#continue looping
    nop

end:
    ori $v0, $0, 10	#exit
    syscall

    .end main
