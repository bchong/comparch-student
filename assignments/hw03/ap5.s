    .set noreorder
    .data
    .text
    .globl main
    .ent main
main:
    addi $a0, $0, 8
    ori $v0, $0, 9	#malloc size of 2 ints (value and pointer)
    syscall

    addi $t0, $0, 1	#$t0=1
    sw $t0, 0($v0)	#store value 1 into $v0 value

    addi $t1, $0, 0	#$t1=0
    sw $t1, 4($v0)	#store 0 into next pointer of $v0

    add $v1, $0, $v0	#$v1=$v0, $v1=head

    addi $a0, $0, 8
    ori $v0, $0, 9	#malloc another int and pointer
    syscall

    addi $t2, $0, 2	#$t2=2
    sw $t2, 0($v0)	#store $t2 into value of new elt

    sw $v1, 4($v0)	#set $v1 next to head

    add $a1, $0, $v0	#$a1=$v0

    lw $t4, 0($a1)	#load head->value into $t4
    add $a0, $t4, $0
    ori $v0, $0, 20	#print hex/dec $t4
    syscall

    lw $t5, 4($a1)	#load head->next to $t5
    lw $t6, 0($t5)	#load $t5->value to $t6
    add $a0, $t6, $0
    ori $v0, $0, 20	#print hex/dec $t6
    syscall

    ori $v0, $0, 10	#exit
    syscall

    .end main

