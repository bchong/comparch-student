    .set noreorder
    .data
    .text
    .globl main
    .ent main
main:
    addi $s0, $0, 84    #score=$s0

    slti $t0, $s0, 90   #if score<90, $t0=1
    bne $t0, $0, Elif1  #if $t0 is not equal to 0, go to Elif1
    nop
    addi $s1, $0, 4     #grade=$s1=4
    j End	      	#jump to end if this segment has been run
    nop

Elif1:
    slti $t0, $s0, 80   #if score<80, $t0=1
    bne $t0, $0, Elif2  #if $t0 not equal to 0, go to Elif2
    nop
    addi $s1, $0, 3     #grade=$s1=3
    j End		#jump to end
    nop

Elif2:
    slti $t0, $s0, 70	#if score<70, $t0=1
    bne $t0, $0, Else	#if $t0 not equal to 0, go to Else
    nop
    addi $s1, $0, 2    #grade=$s1=2
    j End
    nop

Else:
    add $s1, $0, 0     #grade=$s1=0

End:
    add $a0, $0, $s1   
    ori $v0, $0, 20    #print $s1 in hex and dec
    syscall

    	ori $v0, $0, 10    #exit
    	syscall
    .end main

