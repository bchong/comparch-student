    .set noreorder
    .data
    .text
    .globl main
    .ent main
main:
    addi $a0, $0, 8
    ori $v0, $0, 9	#malloc size of 2 ints (8) for instance of struct
    syscall

    addi $t0, $0, 100	#$t0=100
    sw $t0, 0($v0)	#store value 100 into field0 of struct

    addi $t1, $0, -1    #$t1=-1
    sw $t1, 4($v0)	#store calue -1 into field1 of struct

    ori $v0, $0, 10	#exit
    syscall

    .end main
