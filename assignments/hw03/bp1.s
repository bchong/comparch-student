    .set noreorder
    .data
        A: .word 0:8
    .text

    #print function
    .globl print
    .ent print
print:
    ori $v0, $0, 20	#print hex/dec
    syscall
    jal loop
    nop

.end print

    #main function
    .globl main
    .ent main
main:
    lui $t0, %hi(A)
    ori $t0, $t0, %lo(A)

    addi $t1, $0, 1	#$t1=1
    sw $0, 0($t0)	#store 0 into first value of A -> A[0]=0
    sw $t1, 4($t0)	#store 1 into second value of A -> A[1]=1
    addi $s0, $0, 0	#$s0=i=0

loop_conditional:
    slti $t2, $s0, 6	#if $s0 less than 6, then $t2=1
    beq $t2, $0, end	#if #t2=0, go to end. Else, continue
    nop

    sll $t3, $s0, 2	#shift to correct index in array A
    add $t4, $t0, $t3	#$t4=$t0(initial array pos) + $t3(shifted)
    lw $t5, 0($t4)	#$t5=A[i-1]
    lw $t6, 4($t4)	#$t5=A[i-2]
    add $t7, $t6, $t5	#$t7=A[i]
    sw $t7, 8($t4)	#store A[i] in correct position in array
    add $a0, $0, $t7

    jal print		#print A[i]
    nop

loop:
    addi $s0, $s0, 1
    j loop_conditional    #continue loop
    nop

end:
    ori $v0, $0, 10	#exit
    syscall

    .end main
