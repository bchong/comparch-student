    .set noreorder
    .data
    	A: .word 0, 1, 2, 3, 4, 5, 6, 7
    .text
    .globl main
    .ent main
main:
    lui $t0, %hi(A)
    ori $t0, $t0, %lo(A)

    addi $s0, $0, 0     #int sum=0   -----   $s0=sum
    addi $s1, $0, 0     #int i=0     -----   $s1=i
loop:
    slti $t1, $s1, 8    #if i<8      -----   $t1=1 if true, 0 if false
    beq $t1, $0, end    #if $t1==0, go to end
    nop
    sll $t3, $s1, 2	#shift $s1 left by 2 and save into $t3
    add $t4, $t0, $t3	#add shifted $t3 to $t0
    lw $t2, 0($t4)	#load A[i] into $t2
    add $s0, $s0, $t2	#sum=sum+A[i]

end:			
    ori $v0, $0, 1		#printf
    syscall
